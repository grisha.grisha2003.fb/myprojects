﻿#include <Windows.h>
#include <iostream>
#include <chrono>
#include<vector>
#include<algorithm>

int main()
{
	// Карта
	int nMapHeight = 16;
	int nMapWidth = 16;

	std::wstring map;
	map += L"################";
	map += L"#..............#";
	map += L"#.....##.......#";
	map += L"#.......#......#";
	map += L"#........#.....#";
	map += L"#.........#....#";
	map += L"#..........#...#";
	map += L"#...........#..#";
	map += L"#...........#..#";
	map += L"#...........#..#";
	map += L"#...........#..#";
	map += L"#..............#";
	map += L"#...........#..#";
	map += L"#...........#..#";
	map += L"#..............#";
	map += L"################";

	//Player
	int nScreenWidth = 120;
	int nScreenHeight = 40;

	float fPlayerX = 1.0f;
	float fPlayerY = 1.0f;
	float fPlayerA = 0.0f;
	
	float fFOV = 3.1415 / 3;
	float fDepth = 30.0f;

	auto tp1 = std::chrono::system_clock::now(); // Переменные для подсчёта
	auto tp2 = std::chrono::system_clock::now(); // пройденого времени

	//Буффер вывода 
	wchar_t* screen = new wchar_t[nScreenWidth * nScreenHeight + 1]; // Массив для записи в буфер
	HANDLE hConsole = CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE, 0, NULL, CONSOLE_TEXTMODE_BUFFER, NULL); // Буфер экрана
	SetConsoleActiveScreenBuffer(hConsole); // Настройка консоли
	DWORD dwBytesWritten = 0; // Для дебага


	while (1)  // Игровой цикл
	{

		tp2 = std::chrono::system_clock::now();
		std::chrono::duration <float> elapsedTime = tp2 - tp1;
		tp1 = tp2;
		float fElapsedTime = elapsedTime.count();

		if (GetAsyncKeyState((unsigned short)'A') & 0x8000)
			fPlayerA -= (1.5f) * fElapsedTime; // Поворот по часовой стрелке нажатием на клавишу "А"

		if (GetAsyncKeyState((unsigned short)'D') & 0x8000)
			fPlayerA += (1.5f) * fElapsedTime; // Поворот против часовой стрелке нажатием на клавишу "D"

		if (GetAsyncKeyState((unsigned short)'W') & 0x8000)
		{
			fPlayerX += sinf(fPlayerA) * 5.0f * fElapsedTime;
			fPlayerY += cosf(fPlayerA) * 5.0f * fElapsedTime;
			
			if (map[(int)fPlayerY * nMapWidth + (int)fPlayerX] == '#')
			{
				fPlayerX -= sinf(fPlayerA) * 5.0f * fElapsedTime;
				fPlayerY -= cosf(fPlayerA) * 5.0f * fElapsedTime;
			}
			
		};
		if (GetAsyncKeyState((unsigned short)'S') & 0x8000)
		{
			fPlayerX -= sinf(fPlayerA) * 5.0f * fElapsedTime;
			fPlayerY -= cosf(fPlayerA) * 5.0f * fElapsedTime;

			if (map[(int)fPlayerY * nMapWidth + (int)fPlayerX] == '#')
			{
				fPlayerX += sinf(fPlayerA) * 5.0f * fElapsedTime;
				fPlayerY += cosf(fPlayerA) * 5.0f * fElapsedTime;
			}
		};

		//Ray cast

		for (int x = 0; x < nScreenWidth; x++) //проходим по всем x
		{
			float fRayAngle = (fPlayerA - fFOV / 2.0f) + ((float)x / (float)nScreenWidth) * fFOV; //Направление луча

			//Находим расстояние до стенки в направлении fRayAngle

			float fDistanceToWall = 0.0f; 
			bool bHitWall = false;

			float fEyeX = sinf(fRayAngle); // Коордиаты единичного вектора fRayAngle
			float fEyeY = cosf(fRayAngle);

			while (!bHitWall && fDistanceToWall < fDepth)
			{
				fDistanceToWall += 0.1f;

				int nTestX = (int)(fPlayerX + fEyeX * fDistanceToWall); // Точка на игровом поле
				int nTestY = (int)(fPlayerY + fEyeY * fDistanceToWall); // в которую попал луч

				if (nTestX < 0 || nTestX >= nMapWidth || nTestY < 0 || nTestY >= nMapHeight) // Если вышли за зону
				{
					bHitWall = true;
					fDistanceToWall = fDepth;
				}
				else if (map[nTestY * nMapWidth + nTestX] == '#')
					bHitWall = true;

				// Поиск ребер
				bool bBoundary = false;
				std::vector <std::pair <float, float>> p;

				for (int tx = 0; tx < 2; tx++)
					for (int ty = 0; ty < 2; ty++) // Проходим по всем 4м рёбрам
					{
						float vx = (float)nTestX + tx - fPlayerX; // Координаты вектора,
						float vy = (float)nTestY + ty - fPlayerY; // ведущего из наблюдателя в ребро
						float d = sqrt(vx * vx + vy * vy); // Модуль этого вектора
						float dot = (fEyeX * vx / d) + (fEyeY * vy / d); // Скалярное произведение (единичных векторов)
						p.push_back(std::make_pair(d, dot)); // Сохраняем результат в массив
					}

				// Мы будем выводить два ближайших ребра, поэтому сортируем их по модулю вектора ребра
				std::sort(p.begin(), p.end(), [](const std::pair <float, float>& left, const std::pair <float, float>& right) {return left.first < right.first; });

				float fBound = 0.005; // Угол, при котором начинаем различать ребро.
				if (acos(p.at(0).second) < fBound) bBoundary = true;
				if (acos(p.at(1).second) < fBound) bBoundary = true;

				// Вычисляем координаты начала и конца стенки

				int nCeiling = (float)(nScreenHeight / 2.0) - nScreenHeight / ((float)fDistanceToWall);
				int nFLoor = nScreenHeight - nCeiling;

				short nShade;

				if (bBoundary == true)								nShade = '|';
				else if (fDistanceToWall <= fDepth / 3.0f)			nShade = 0x2588; // Если стена близко, то рисуем
				else if (fDistanceToWall < fDepth / 2.0f)			nShade = 0x2593; // светлую полоску
				else if (fDistanceToWall < fDepth / 1.5f)			nShade = 0x2591; // Для отдалённых участков
				else if (fDistanceToWall < fDepth)					nShade = 0x2591; // рисуем более тёмную
				else												nShade = ' ';
				
				for (int y = 0; y < nScreenHeight; y++) // При заданом Х проходим все Y
				{
					if (y <= nCeiling)
						screen[y * nScreenWidth + x] = ' ';
					else if (y > nCeiling && y <= nFLoor)
						screen[y * nScreenWidth + x] = nShade;
					else
					{
						// То же самое с полом - более близкие части рисуем более заметными символами
						float b = 1.0f - ((float)y - nScreenHeight / 2.0) / ((float)nScreenHeight / 2.0);
						if (b < 0.25)        nShade = '#';
						else if (b < 0.5)    nShade = 'x';
						else if (b < 0.75)   nShade = '~';
						else if (b < 0.9)    nShade = '-';
						else                 nShade = ' ';

						screen[y * nScreenWidth + x] = nShade;
					}
				}
			}
		}
		WriteConsoleOutputCharacter(hConsole, screen, nScreenWidth * nScreenHeight, { 0, 0 }, &dwBytesWritten);
	}
	return 0;
}
